const visualiseOrderProducts = () => {
  const cartProducts = Object.values(
    JSON.parse(sessionStorage.getItem("cartProducts")) || {}
  );
  const totalPrice = calculatePrice(cartProducts);
  const productsContainer = document.getElementById("orderContainer");
  productsContainer.innerHTML = "";
  const productsDiv = create("div", { class: "productsHolder" });

  cartProducts.forEach(cartProduct =>
    productsDiv.append(addCartProduct(cartProduct, visualiseOrderProducts))
  );
  const total = create("p", {
    class: "total",
    text: `Общо: ${totalPrice} лв.`
  });

  productsContainer.appendChild(productsDiv);
  productsContainer.appendChild(total);
};

const errorMessages = {
  name: "Моля въведете валидни име и фамилия.",
  email: "Моля въведете валиден имейл адрес.",
  phoneNumber: "Моля въведете валиден телефонен номер.",
  address: "Адресът за доставка не е въведен.",
  submit: "Въведени са невалидни данни.",
  success: "Поръчката е направена успешно."
};

const validators = {
  name: name => name.split(" ").length > 1,
  email: email => new RegExp(/\S+@\S+\.\S+/).test(email),
  phoneNumber: phoneNumber =>
    (phoneNumber.length == 10 &&
      phoneNumber.startsWith("08") &&
      new RegExp(/^\d+$/).test(phoneNumber.slice(2))) ||
    (phoneNumber.length == 13 &&
      phoneNumber.startsWith("+359") &&
      new RegExp(/^\d+$/).test(phoneNumber.slice(3))),
  address: address => true
};

let areValid = {
  name: false,
  email: false,
  phoneNumber: false,
  address: false
};

const isNotEmpty = value => value != "";

const validateField = (key, fieldValue) => {
  const value = fieldValue.trim();
  if (!(isNotEmpty(value) && validators[key](value))) {
    return false;
  }
  return true;
};

const validateBody = body => Object.values(body).every(value => value);

visualiseOrderProducts();

const orderForm = document.getElementById("orderForm");
orderForm.onsubmit = e => {
  e.preventDefault();
  const body = {
    name: document.getElementById("name").value,
    email: document.getElementById("email").value,
    phoneNumber: document.getElementById("phoneNumber").value,
    address: document.getElementById("address").value
  };
  const errorMessage = document.getElementById("notificationInvalidForm");

  if (validateBody(body)) {
    errorMessage.innerHTML = errorMessages["success"];
    setTimeout(() => {
      sessionStorage.setItem("cartProducts", JSON.stringify({}));
      orderForm.submit();
    }, 2000);
  } else {
    errorMessage.innerHTML = errorMessages["submit"];
  }

  removeClass(errorMessage, "hiddenNotification");
  addClass(errorMessage, "showNotification");
  setTimeout(() => {
    removeClass(errorMessage, "showNotification");
    addClass(errorMessage, "hiddenNotification");
  }, 2000);
};

const inputFields = document.getElementsByTagName("input");

Array.prototype.forEach.call(inputFields, field => {
  field.addEventListener("blur", e => {
    if (!validateField(field.name, field.value)) {
      areValid[field.name] = false;
      const errorMessage = document.getElementById("notificationInvalidForm");
      errorMessage.innerHTML = errorMessages[field.name];
      removeClass(errorMessage, "hiddenNotification");
      addClass(errorMessage, "showNotification");

      addClass(field, "error");
      setTimeout(() => {
        removeClass(errorMessage, "showNotification");
        addClass(errorMessage, "hiddenNotification");
      }, 2000);
    }
  });
  field.addEventListener("focus", e => {
    areValid[field.name] = true;
    removeClass(field, "error");
  });
});
