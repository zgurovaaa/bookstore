let allNavigationTabs = document.getElementsByClassName("navLink");

const getTabs = tabName => tabs[tabName];

const addProductsToDom = (data, tab) => {
  const productContainer = document.getElementById("productsContainer");
  const tabProductsDiv =
    document.getElementById(tab) ||
    create("div", { class: "tabProducts", id: tab });

  for (var index = 0; index < 4; index++) {
    tabProductsDiv.appendChild(addProductToDom(data[index]));
  }

  productContainer.appendChild(tabProductsDiv);
};

const replaceTabs = newTabs => {
  const productContainer = document.getElementById("productsContainer");
  productContainer.innerHTML = "";

  newTabs.forEach(subTab => {
    getProductsByCategory(subTab.jsonName).then(data => {
      const products = JSON.parse(sessionStorage.getItem("products")) || {};
      products[subTab.tab] = data;
      sessionStorage.setItem("products", JSON.stringify(products));
      addProductsToDom(data, subTab.tab);
    });
  });
};

const clickedTab = tab => {
  const previousTab = document.getElementsByClassName("navClickedOn")[0];
  removeClass(previousTab, "navClickedOn");
  addClass(tab, "navClickedOn");
};

Array.prototype.map.call(allNavigationTabs, tab => {
  tab.onclick = e => {
    clickedTab(tab);
    replaceTabs(getTabs(e.currentTarget.innerText));
  };
});

const currentTab = document.getElementsByClassName("navClickedOn")[0];
clickedTab(currentTab);
replaceTabs(getTabs(currentTab.innerText));
