function getProductsByCategory(lowerSection) {
  return new Promise(function(success, fail) {
    var request = new XMLHttpRequest();
    request.open("GET", "../jsonfiles/" + lowerSection + ".json", true);
    request.send(null);

    request.addEventListener(
      "load",
      function() {
        if (
          (request.status >= 200 && request.status <= 299) ||
          request.status == 304
        ) {
          success(JSON.parse(this.responseText));
        } else {
          fail(new Error("Request failed: " + request.statusText));
        }
      },
      false
    );
  });
}
