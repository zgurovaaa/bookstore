const changeCount = (productKey, newCount, cb) => {
  let cartProducts = JSON.parse(sessionStorage.getItem("cartProducts")) || {};
  if (newCount > 0 && newCount <= 100) {
    cartProducts[productKey].count = newCount;
  } else if (newCount == 0) {
    delete cartProducts[productKey];
  }
  sessionStorage.setItem("cartProducts", JSON.stringify(cartProducts));
  cb();
};

const visualiseCartProducts = () => {
  const cartProducts = Object.values(
    JSON.parse(sessionStorage.getItem("cartProducts")) || {}
  );
  const totalPrice = calculatePrice(cartProducts);

  const productsContainer = document.getElementById("cartProducts");
  productsContainer.innerHTML = "";
  const productsDiv = create("div", { class: "productsHolder" });

  removeClass(productsContainer, "hiddenCart");
  addClass(productsContainer, "showCart");

  cartProducts.forEach(cartProduct =>
    productsDiv.append(addCartProduct(cartProduct, visualiseCartProducts))
  );

  const pay = create("button", {
    class: "moreButton",
    text: "Завърши поръчката"
  });

  pay.addEventListener("click", () => {
    if (window.location.pathname === "/bookstore/html/order.html") {
      location.reload();
    } else {
      window.location = "order.html";
    }
  });
  const total = create("p", {
    class: "total",
    text: `Общо: ${totalPrice} лв.`
  });

  productsContainer.appendChild(productsDiv);
  productsContainer.appendChild(total);
  productsContainer.appendChild(pay);
};

const calculatePrice = products => {
  let sum = 0;
  products.forEach(({ product, count }) => {
    const priceSingleProduct = Number(product.price);
    sum += count * priceSingleProduct;
  });
  return Math.round(sum * 100) / 100;
};

const hideCartProducts = () => {
  const productsContainer = document.getElementById("cartProducts");
  removeClass(productsContainer, "showCart");
  addClass(productsContainer, "hiddenCart");
};

const cart = document.getElementById("myCartContent");

cart.addEventListener("click", () => {
  const productsContainer = document.getElementById("cartProducts");
  if (hasClass(productsContainer, "hiddenCart")) {
    visualiseCartProducts();
  } else if (hasClass(productsContainer, "showCart")) {
    hideCartProducts();
  }
});
