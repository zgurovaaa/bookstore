const hasClass = (element, className) => element.classList.contains(className);

const addClass = (element, className) => element.classList.add(className);

const removeClass = (element, className) => element.classList.remove(className);

const toggleClass = (element, className) => element.classList.toggle(className);

const create = (type, attributes) => {
  var element = document.createElement(type);
  if (attributes) {
    for (var attr in attributes) {
      if (attributes.hasOwnProperty(attr)) {
        if (attr == "text") {
          element.innerHTML = attributes[attr];
        } else {
          element.setAttribute(attr, attributes[attr]);
        }
      }
    }
  }
  return element;
};

const checkPrice = (from, to) => element =>
  parseInt(element.price) >= from && parseInt(element.price) <= to;

const addProductToDom = product => {
  const productDiv = create("div", { class: "productHolder" });
  const productImg = create("img", {
    class: "imageProduct",
    src: `../../../${product.img}`
  });
  const productDetails = create("div", { class: "productDetails" });
  const productTitle = create("h2", {
    class: "productTitle",
    text: product.title
  });
  const productAuthor = create("h3", {
    class: "productAuthor",
    text: product.author
  });
  const productPrice = create("p", {
    class: "productPrice",
    text: `Цена: ${product.price} лв.`
  });
  const productPages = create("p", {
    class: "productPages",
    text: `Страници: ${product.pages}`
  });
  const productDescription = create("p", {
    class: "productDescription",
    text: product.description
  });

  productDiv.appendChild(productImg);
  productDetails.appendChild(productTitle);
  productDetails.appendChild(productAuthor);
  productDetails.appendChild(productDescription);
  productDetails.appendChild(productPages);
  productDetails.appendChild(productPrice);

  const buyButton = create("button", {
    class: "buyButton",
    text: "КУПИ"
  });

  buyButton.addEventListener("click", e => {
    let cartProducts = JSON.parse(sessionStorage.getItem("cartProducts")) || {};
    cartProducts[product.title] = {
      product,
      count: cartProducts[product.title]
        ? cartProducts[product.title].count + 1
        : 1
    };
    sessionStorage.setItem("cartProducts", JSON.stringify(cartProducts));

    const notificationDiv = document.getElementById("notification");
    removeClass(notificationDiv, "hiddenNotification");
    addClass(notificationDiv, "showNotification");
    setTimeout(() => {
      removeClass(notificationDiv, "showNotification");
      addClass(notificationDiv, "hiddenNotification");
    }, 2000);
  });
  productDetails.appendChild(buyButton);
  productDiv.appendChild(productDetails);
  return productDiv;
};

const addCartProduct = ({ product, count }, callback) => {
  console.log(product.title);
  const productDiv = create("div", { class: "productHolderCart" });
  const productImg = create("img", {
    class: "imageProduct",
    src: `../../../${product.img}`
  });
  const productTitle = create("p", {
    class: "productTitle",
    text: product.title
  });
  const priceContainer = create("div", { class: "priceContainer" });
  const productPrice = create("p", {
    class: "productPrice",
    text: `${product.price} лв.`
  });
  const increaseButton = create("button", {
    class: "countButtons",
    text: "+"
  });
  const deleteButton = create("button", {
    class: "countButtons",
    text: "х"
  });
  const countProduct = create("span", {
    class: "countProduct",
    text: count
  });
  const decreaseButton = create("button", {
    class: "countButtons",
    text: "-"
  });

  deleteButton.addEventListener("click", e =>
    changeCount(product.title, 0, callback)
  );
  increaseButton.addEventListener("click", e =>
    changeCount(product.title, count + 1, callback)
  );
  decreaseButton.addEventListener("click", e =>
    changeCount(product.title, count - 1, callback)
  );

  productDiv.appendChild(productImg);
  productDiv.appendChild(productTitle);
  priceContainer.appendChild(decreaseButton);
  priceContainer.appendChild(countProduct);
  priceContainer.appendChild(increaseButton);
  priceContainer.appendChild(productPrice);

  const div = create("div", { class: "productCartInfo" });
  div.appendChild(deleteButton);
  div.appendChild(productDiv);
  div.appendChild(priceContainer);
  return div;
};
