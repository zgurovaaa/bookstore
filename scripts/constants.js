var tabs = {
  "Биографии": [
    { tab: "Биографии", jsonName: "biography" },
  ],
  "Икономика": [
    { tab: "Икономика", jsonName: "economy" },
  ],
  "Крими, Мистерия": [
    { tab: "Крими, Мистерия", jsonName: "mistery" },
  ],
  "Маркетинг и мениджмънт": [
    { tab: "Маркетинг и мениджмънт", jsonName: "marketing" },
  ],
  "Психология": [
    { tab: "Психология", jsonName: "psychology" },
  ]
};